﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class StarPoint : MonoBehaviourPun
{
    public int starPoint=1;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Test other :"+other.gameObject.name);
        if(other.gameObject.CompareTag("Player"))
        {
            PunStar otherHeal=other.gameObject.GetComponent<PunStar>();
            otherHeal.StarPoint(starPoint);

            //send RPC To owner PickupStar
            photonView.RPC("PunRPCStarPoint",RpcTarget.MasterClient);
        }
    }
    [PunRPC]
    private void PunRPCStarPoint()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        if(!photonView.IsMine)
        return;
        PhotonNetwork.Destroy(this.gameObject);
    }
    
}
