﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PunStar : MonoBehaviourPunCallbacks,IPunObservable
{
    public const int maxStar=100;
    public int currentStar=maxStar;

    public void OnGui()
    {
        if(photonView.IsMine)
        GUI.Label(new Rect(0,0,300,50),"Player Star:"+currentStar);
    }
    public void TakeDamage(int amount,int OwnerNetID)
    {
        if(photonView !=null)
        photonView.RPC("PunRPCTakeDamage",RpcTarget.All,amount,OwnerNetID);
        else print("photonView is NULL.");
    }
    [PunRPC]
    public void PunRPCTakeDamage(int amount,int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentStar -=amount;
        if(currentStar<=0)
        {
            currentStar=maxStar;
        }
    }
    public void OnPhotonSerializeView(PhotonStream stream,PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(currentStar);
        }
        else{
            currentStar=(int)stream.ReceiveNext();
        }
    }
    public void StarPoint(int amount)
    {
        currentStar +=amount;
    }
}
