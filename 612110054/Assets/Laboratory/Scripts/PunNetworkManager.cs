﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;

    bool isGameStart=false;
    bool isFirstSetting=false;
    public bool isGameOver = false;
    public GameObject StarPointPrefab;
    public int numberOfStar=5;
    float m_count=0;
    public float m_CountDownDropStar=10;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;
     public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    private void Awake()
    {
        singleton = this;
        OnPlayerSpawned += SpawnPlayer;
        OnFirstSetting += FirstSettingAirDropHealing;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Camera.main.gameObject.SetActive(false);

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }
    private void FirstSettingAirDropHealing()
    {
        isFirstSetting = true;

        int half = numberOfStar / 2;
        for (int i = 0; i < half; i++)
        {
            PhotonNetwork.InstantiateRoomObject(StarPointPrefab.name
                                , AirDrop.RandomPosition(5f)
                                , AirDrop.RandomRotation()
                                , 0);
        }
        m_count = m_CountDownDropStar;
    }

    private void AirDropStarPoint()
    {
        
        
        if(GameObject.FindGameObjectsWithTag("StarPoint").Length<numberOfStar)
        {
            m_count -=Time.deltaTime;
            if(m_count<=0)
            {
                m_count=m_CountDownDropStar;
                PhotonNetwork.Instantiate(StarPointPrefab.name
                ,AirDrop.RandomPosition(10f)
                ,AirDrop.RandomRotation()
                ,0);
            }
        }
        
        
    }
    private void Update()
    {
        if(PhotonNetwork.IsMasterClient !=true)
        return;
        if(isGameStart==true)
        {
            if (isFirstSetting == false)
                OnFirstSetting();
            if(isFirstSetting==true)
                AirDropStarPoint();
            
        }
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        isGameStart=true;
    }
}
